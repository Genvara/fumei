---
title: "The battle for being the superior Metaverse."
date: 2021-10-28T00:00:00+00:00
draft: false
---
(Edited on Nov 4th, 2022)

The Metaverse is a buzzword that we hear all the time currently, especially in the tech industry, and it means the virtual reality space in which users can interact with a computer-generated environment and with other users in it.

That sounds like a neat idea, does it not? Initially, you might have conceived virtual reality games, since they already are kind of do represent that concept, in reality, games such as VRChat, or Rec Room, however, it is more than just that, it is the next biggest thing a lot of tech companies, including silicon valley companies, are betting on, call it the "silicon valley tech dream" if you will.

## Which companies are interested in it so far?
Meta, ROBLOX, Epic Games, and probably more, are making the grounds for the Metaverse.

## How?
Starting with initially calling themselves "Metaverse Companies", after all, it is the reason the parent company Meta (previously Facebook) changed their company name, in [Mark Zuckerberg's own words in this Verge article](https://www.theverge.com/22588022/mark-zuckerberg-facebook-ceo-metaverse-interview) about the name change: "Facebook would strive to build a maximalist, interconnected set of experiences straight out of sci-fi — a world known as the **Metaverse** ... We will effectively transition from people seeing us as a primarily being a social media company to being **a Metaverse company**."

## Why now?
We have reached the starting point of the Metaverse, or at least that's what Matthew Bell thinks, especially [since he has set the key characteristics of the Metaverse](https://www.matthewball.vc/all/themetaverse), so now companies know what a Metaverse looks like, and are now building their own based on those base characteristics.


## Multiple Metaverses?
*** all right, welcome to the opinions area. Take a seat.***
Yes, since multiple companies are fine with having over one Metaverse I guess, and with ROBLOX existing, also Tim Sweeney taking Fortnite's "opportunity" of becoming one, and Brendan Greene wanting to do the same thing as his next project, and the big one, Meta.

## So, which one is the superior one?
I think Meta already got a lead, and there're factors to that.
### 1. Being massively known and used, almost everywhere.
By being known in multiple parts of the world, if you ask anyone in your country if they have a social media other than Meta, they'd say none, or they would say Meta's other services (Instagram, WhatsApp) thinking they are not apart from their ecosystem, and will barely hear anything about Twitter, or other social media, or so that's the case in my country, while this might not be the case for you, foreign reader, it is in some parts of the world, especially in third-world-countries.
### 2. Good infrastructure to do so.
Meta's infrastructure is one of the big tech infrastructures that covers the entire world and is bigger than ROBLOX's and Epic Games combined because those two companies rely on Amazon’s AWS and EC2.
### 3. Many people depend on it so much, they often forget they are using the internet to access it.
Especially in Africa, I will not explain this one, and let [Quartz's article explain it.](https://qz.com/333313/milliions-of-facebook-users-have-no-idea-theyre-using-the-internet/)

## is this a good thing?
Short answer: **NO, NOT AT ALL.**

Long answer: despite having all the powers to pull off something like this, we must make a note of Meta's shady practices and accidents, how they are dealing with oculus, and the shady and scary data collection work they do (just look up the antitrust lawsuits), the sudden data breaches that leak multiple information, the recent outages, [launching nonprofit organizations that isn't](https://www.computerworld.com/article/3032646/the-surprising-truth-about-facebooks-internetorg.html) and wanting to become **THE internet**, and also wanting to **E**mbrace **E**xtend **E**xtinguish the entire internet too.

But this is all just rambling, we'll have to see what happens in the future, thanks for coming to my TED Talk.
