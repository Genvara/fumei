---
title: "Welcome (back?)!"
date: 2022-03-16T17:06:10+01:00
draft: false
---

Welcome to fumei.

A personal blog with no intention for anyone else to read it except whoever finds it, and I will never mention it or the blog posts in it except if it’s for reference.

# What happened to the old blog?

Didn’t like it, plus it was way too rushed and I gave zero care for it.

# What’s the difference between the old one and the new one?

The way how it looks, I guess? I wanted to change the theme, grabbed a random ass theme from the Hugo theme library, and called it a day, I might customize it and make it have a feel rather than its bland state that it is at the moment, but that is for another day.

Oh, and migrated to GitLab, GitHub had too many growing pains.

# And the old blog post(s)?

There was only one, and I don’t even consider it a blog post. It’s just a 30-minute written report, in a time when Meta poisoned the term ‘Metaverse’.

However, yes, it’s back, I won't change it is contents either.

# Do you have a schedule for upcoming posts?

**No**, posts will happen when I want to, expect huge time gaps between them however… really depends on my dedication to it and my motivation.

# What does fumei mean?

¯\\\_(ツ)\_/¯

Anyhow, if you are new here, welcome and enjoy your stay here, and if you were a previous reader, welcome back!